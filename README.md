# Client Web App
pff

## Build

```
yarn install
```

## Run

### Run Fake API
```
json-server -p 1337 --watch api.json
```

### Run the client
```
yarn start
```
