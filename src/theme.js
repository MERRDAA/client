import getMuiTheme from 'material-ui/styles/getMuiTheme';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import * as Colors from 'material-ui/styles/colors';

const getTheme = () => {
  let overwrites = {
    "palette": {
        "primary1Color": Colors.green500,
        "primary2Color": Colors.green400,
        "accent1Color": Colors.red400
    }
};
  return getMuiTheme(baseTheme, overwrites);
}

export default getTheme
