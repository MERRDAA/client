import React from 'react';
import { jsonServerRestClient, Admin, Resource, Delete, fetchUtils } from 'admin-on-rest';

import {Dashboard} from './resources/dashboard/';
import { UsersList } from './resources/users';
import { SensorsList, SensorsEdit, SensorsCreate } from './resources/sensors';
import { RoomsList, RoomsEdit, RoomsCreate} from './resources/rooms';

import getTheme from './theme'
import RoomIcon from 'material-ui/svg-icons/action/room';
import UserIcon from 'material-ui/svg-icons/social/group';
import SensorIcon from 'material-ui/svg-icons/av/mic';

import authClient from './authClient';

const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    const token = localStorage.getItem('token');
    options.headers.set('Authorization', `${token}`);
    return fetchUtils.fetchJson(url, options);
}

export const restClient=jsonServerRestClient('http://localhost:8080/api', httpClient)

const App = () => (
    <Admin title="Project Shiuu" 
        theme={getTheme()}
        dashboard={Dashboard}
        restClient={restClient}
        authClient={authClient}
    >

        <Resource name="users" icon={UserIcon} list={UsersList} />
        <Resource name="sensors" icon={SensorIcon} remove={Delete} list={SensorsList} edit={SensorsEdit} create={SensorsCreate}/>
        <Resource name="rooms" icon={RoomIcon} remove={Delete} list={RoomsList} edit={RoomsEdit} create={RoomsCreate}/>

    </Admin>
);

export default App;
