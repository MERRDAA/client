import React from 'react';
import { List, Edit, Create, Datagrid, TextField, ReferenceField, EditButton, SimpleForm, DisabledInput, TextInput, ReferenceInput, SelectInput, required} from 'admin-on-rest';

export const RoomsList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="roomId" />
            <TextField source="name" />
            <TextField source="location" />
            <ReferenceField label="Sensor" source="sensor" reference="sensors">
                <TextField source="id" />
            </ReferenceField>
            <EditButton />
        </Datagrid>
    </List>
);


const RoomTitle = ({ record  }) => {
    return <span>Room {record ? `"${record.id}"` : ''}</span>;
};

export const RoomsEdit = (props) => (
    <Edit title={<RoomTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="roomId" />
            <TextInput source="name" />
            <TextInput source="location" />
            <ReferenceInput label="Sensor" source="id" reference="sensors" validate={required}>
                <SelectInput optionText="id" />
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);

export const RoomsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="location" />
            <ReferenceInput label="Sensor" source="id" reference="sensors" validate={required} allowEmpty>
                <SelectInput optionText="id" />
            </ReferenceInput>
        </SimpleForm>
    </Create>
);
