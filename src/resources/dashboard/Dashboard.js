import React from 'react';
import { Card, CardHeader, CardActions, CardMedia, CardText  } from 'material-ui/Card';
import HomeIcon from 'material-ui/svg-icons/action/home';
import CodeIcon from 'material-ui/svg-icons/action/code';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { Grid, Row, Col } from 'react-flexbox-grid';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';

import withWidth from 'material-ui/utils/withWidth';
import { GET_LIST } from 'admin-on-rest';
import { AppBarMobile } from 'admin-on-rest/lib/mui';
import CircularProgress from 'material-ui/CircularProgress';
import LinechartWebSocket from './Linechart'
import LinechartHistory from './LinechartHistory'
import { translate, simpleRestClient  } from 'admin-on-rest';
import { restClient } from '../../App'

let DateTimeFormat;

const styles = {
    welcome: { marginBottom: '2em' },
    flex: { display: 'flex' },
    leftCol: { flex: 1, marginRight: '1em' },
    rightCol: { flex: 1, marginLeft: '1em' },
    singleCol: { marginTop: '2em' },
};

const style_buttons = {
      margin: 12,
};

class Dashboard extends React.Component {

    constructor(props) {
        super(props)
        this.handleChangeOnDropDown = (event, index, value) => this.setState({value_dropdown: value,});
        this.onClickConnect = () => this.linechartws._connect() // do stuff
        this.onClickDisconnect = () => this.linechartws._disconnect() // do stuff
        this.historyFakeIt = () => this.linechartHistory._fake_me_pls()
        this.state = { rooms:[], value_dropdown:null, }
    }

    handleData(data) {
        let result = JSON.parse(data);
        this.setState({count: this.state.count + result.movement});
    }

    componentDidMount(){
        console.log("Não sei JavaScript")
        restClient(GET_LIST, 'rooms', { pagination: { page: 1, perPage: 10 }, sort: { field: 'id', order: 'DESC'  }})
            .then(response => response.data)
            .then(rooms => {
                this.setState({
                    rooms: rooms,
                })
            })
    }

    render() {
        console.log('STATE', this.state)
        const { width } = this.props;
        let rooms = this.state.rooms;
        const room_options = [];
        for (let i = 0; i < rooms.length; i++ ) {
            room_options.push(<MenuItem value={i} key={i} primaryText={rooms[i].name} />);
        }


        return (
            <div>
                <div>
                    <Card>
                        <CardHeader title={'Web Socket Connection'} subtitle={'Very Real Time'} />
                        <CardMedia>
                            <LinechartWebSocket onRef={ref => (this.linechartws = ref)}/>
                        </CardMedia>
                        <CardText>
                            Choose one of the classrooms to Connect in Real Time and see the sound volume of said classroom
                        </CardText>
                        <CardActions>
                            <Grid fluid>
                                <Row>
                                    <Col xs xsOffset={2}>
                                        <SelectField value={this.state.value_dropdown} onChange={this.handleChangeOnDropDown} maxHeight={200}>
                                            {room_options}
                                        </SelectField>
                                    </Col>
                                    <Col xs>
                                        <RaisedButton label="Connect" onClick={this.onClickConnect} style={style_buttons}/>
                                        <RaisedButton label="Disconnect" onClick={this.onClickDisconnect} style={style_buttons}/>
                                    </Col>
                                </Row>
                            </Grid>
                        </CardActions>
                    </Card>
                </div>

                <div style={{marginTop: '30px'}}>
                    <Card>
                        <CardHeader title={'History'} subtitle={'Very Not Real Time'} />
                        <CardMedia>
                            <LinechartHistory onRef={ref => (this.linechartHistory = ref)}/>
                        </CardMedia>
                        <CardText>
                            Choose one of the classrooms to Connect in Real Time and see the sound volume of said classroom
                        </CardText>
                        <CardActions>
                            <Grid fluid>
                                <Row>
                                    <Col xs>
                                        <SelectField value={this.state.value_dropdown} onChange={this.handleChangeOnDropDown} maxHeight={200}>
                                            {room_options}
                                        </SelectField>
                                    </Col>
                                    <Col xs>
                                        <DatePicker
                                            hintText="Date"
                                            DateTimeFormat={DateTimeFormat}
                                            okLabel="Pick"
                                            cancelLabel="Cancel"
                                            locale="en-US"
                                        />
                                   </Col>
                                    <Col xs>
                                        <TimePicker
                                            format="24hr"
                                            hintText="Hour"
                                        />
                                   </Col>
                                    <Col xs>
                                        <RaisedButton label="Retrieve" onClick={this.historyFakeIt} style={style_buttons}/>
                                    </Col>
                                </Row>
                            </Grid>
                        </CardActions>
                    </Card>
                </div>
            </div>
        );
    }
}

export default withWidth()(Dashboard);
