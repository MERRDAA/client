import React from 'react';

import { ResponsiveContainer, LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';

const data1 = [
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
];

class LinechartHistory extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            data: data1,
        };

        this.setState = this.setState.bind(this);
        this.props.onRef(this)

   }
    _generate_graph(e){
        let i = 0
        while(i < 20){
            var randomnumber = Math.floor(Math.random()*100) + 1;
            data1[i] = {name: ' ', db: randomnumber}
            console.log(data1)
            i = i+1
        }
        e.setState({data:data1,})
        console.log("done pila");
    }

    _fake_me_pls() {
        setTimeout(this._generate_graph(this),2000)
    }


    render() {
        return (
            <div>
              <ResponsiveContainer width="100%" height={300}>
                <LineChart data={this.state.data.slice()}
                    margin={{top: 5, right: 30, left: 30, bottom: 10}}>
                  <XAxis dataKey="name"/>
                  <YAxis/>
                  <CartesianGrid strokeDasharray="3 3"/>
                  <Tooltip/>
                  <Legend />
                  <Line type="monotone" dataKey="db" stroke="#82ca9d"/>
                </LineChart>
              </ResponsiveContainer>
            </div>
       );
    }
}

export default (LinechartHistory);
