import React from 'react';
import SockJsClient from 'react-stomp';

import { ResponsiveContainer, LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';

const data1 = [
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
      {name: ' ', db: 0},
];

class LinechartWebSocket extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            data: data1,
            show: false,
        };

        this.setState = this.setState.bind(this);
        this.props.onRef(this)
   }

    _initialize() {
        console.log("connected");
    }

    _onMessage(e) {
        console.log('Received:', e);
        let currData = this.state.data;
        currData.shift()
        currData.push({name:" ", db: parseInt(e, 10)})
        if(this.state.show){
          if (parseInt(e, 10) > 95)
            window.alert("Room exceeded Sound Maximum")
          this.setState({data:currData,})
        }
    }

    _onClose() {
        console.log("onClose");
    }

    _connect() {
        console.log("test")
        this.setState({show:true,})
    }
    _disconnect(){
        this.setState({show:false,})
    }

    render() {
        return (
            <div>
              <SockJsClient url='http://localhost:8080/ws' topics={['/topic/realTime']}
                        onConnect={()    => { console.log("Connected"); }}
                        onMessage={(msg) => this._onMessage(msg) }
                        ref={ (client) => { this.clientRef = client }} />
                <ResponsiveContainer width="100%" height={300}>
                <LineChart data={this.state.data.slice()}
                    margin={{top: 5, right: 30, left: 30, bottom: 10}}>
                  <XAxis dataKey="name"/>
                  <YAxis/>
                  <CartesianGrid strokeDasharray="3 3"/>
                  <Tooltip/>
                  <Legend />
                  <Line type="monotone" dataKey="db" stroke="#82ca9d"/>
                </LineChart>
              </ResponsiveContainer>
            </div>
       );
    }
}

export default (LinechartWebSocket);
