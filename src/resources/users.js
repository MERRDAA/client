import React from 'react';
import { List, Datagrid, TextField, EmailField } from 'admin-on-rest';

export const UsersList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="userId" />
            <TextField source="name" />
            <EmailField source="email" />
            <TextField source="type" />
        </Datagrid>
    </List>
);

