import React from 'react';
import { Create, Edit, List, Datagrid, NumberField, TextField, EditButton, SimpleForm, DisabledInput, TextInput} from 'admin-on-rest';

export const SensorsList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="type" />
            <NumberField source="sid" />
            <EditButton />
        </Datagrid>
    </List>
);

const SensorTitle = ({ record  }) => {
    return <span>Sensor {record ? `"${record.id}"` : ''}</span>;
};

export const SensorsEdit = (props) => (
    <Edit title={<SensorTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="type" />
            <TextInput source="sid" />
        </SimpleForm>
    </Edit>
);

export const SensorsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="type" />
            <TextInput source="sid" />
        </SimpleForm>
    </Create>
);
